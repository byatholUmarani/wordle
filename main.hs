import System.Random ( randomRIO )
import Data.List ( delete )


filterCorrectWords :: String -> String -> [String] -> [String]
filterCorrectWords guessedWord response  =  filter  (\listWord -> all (\(respLetter, guessedLetter, letter) -> (respLetter == 'G' && guessedLetter == letter) || (respLetter=='Y' && elem guessedLetter listWord && guessedLetter/=letter) || (respLetter=='B' && notElem guessedLetter listWord)) $ zip3 response guessedWord listWord)

wordle' :: String -> String -> Int ->[String] -> IO ()
wordle' word response chances wordsList
    | any (`notElem` "GYB") response = do
            putStrLn "Enter Correct response in G Y B only"
            wordle wordsList chances
    | otherwise = do
            if all (=='G') response then putStrLn "Computer Won" 
            else do 
                putStrLn("You have " ++ show (chances-1) ++ " chances more")
                wordle (filterCorrectWords word response (delete word wordsList)) (chances-1)

wordle :: [String] -> Int -> IO ()
wordle wordsList chances
    | chances == 0 = putStrLn "You Won / Computer Lost"
    | null wordsList = putStrLn "No words in the list"
    | otherwise = do
        randomNum <- randomRIO (0,length wordsList-1)
        let word = wordsList !! randomNum
        putStrLn word
        putStrLn "Enter your response as G Y B" 
        response <- getLine
        wordle' word response chances wordsList

main :: IO ()
main = do
    wordsFile <- readFile "5words.txt"
    let wordsList = lines wordsFile
    wordle wordsList 6

